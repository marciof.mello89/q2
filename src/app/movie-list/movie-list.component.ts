import { HttpClient } from '@angular/common/http';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {
  movies : Movie[] = [];
  year = '';

  constructor(private http: HttpClient) {

  }

  ngOnInit() {
  }

searchMovies() {
  console.log(this.year);
  if(this.year !== null && this.year !== '') {
    return this.http.get<any>(`https://jsonmock.hackerrank.com/api/movies?Year=${this.year}`)
                    .subscribe(data => {
                      if(data){
                      this.movies = data.data;
                      }
                    });
  }
}

}

export interface Movie {
  Title: string;
  Year: number;
  imdbID: number;
}
